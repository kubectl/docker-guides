# Avoid source code within production docker images

It is usually a good practice that production images do not contain dev tools or even source code.
To achieve this with docker mechanisms, use multi-stage dockerfiles.
Copy your code to the stage "development", compile it there, and then just copy the binary from there to the "production" image.
To test the Dockerfile check at the end for source files in the resulting image.
